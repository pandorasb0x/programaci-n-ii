# Generales acerca del proyecto:


* **Archivo CSV**

En la clase "**Constantes**" la cual está en la ruta: **src/misUtilidades**, 
se encuentra: **public final static String RUTA_ARCHIVO = "src/Materias.csv"**;
Ahí es donde le seteamos el path en donde está el archivo .csv que vamos a 
utilizar. 

**IMPORTANTE:** El path relativo que estamos usando, puede variar de IDE a IDE. 
En este caso, es el path relativo correcto para IntelliJ. Para que en Eclipse no
arroje error, debe cambiarse por **Materias.csv**

* **TDAs**

Elegimos una implementación dinámica para el mejor aprovechamiento del proceso 
de los datos del archivo que consumimos.
Por ahora, están todas las interfaces e implementaciones vistas en clase.
La idea es, una vez terminado el proyecto, borrar aquellas que no se usen.


