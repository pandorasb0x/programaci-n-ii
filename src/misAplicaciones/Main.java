package misAplicaciones;

import misApis.ConjuntoTDA;
import misApis.DiccionarioMultipleTDA;
import misApis.GrafoTDA;
import misImplementaciones.dinamicas.Conjunto;
import misImplementaciones.dinamicas.DiccionarioMultiple;
import misImplementaciones.dinamicas.Grafo;
import misMetodos.MetodosConjunto;
import misMetodos.MetodosFuncionales;
import misMetodos.MetodosGrafo;
import misUtilidades.Constantes;

public class
Main {
	public static void main(String[] args) {
		
		DiccionarioMultipleTDA registroDeMaterias = new DiccionarioMultiple();
		DiccionarioMultipleTDA materiasCorrelativas = new DiccionarioMultiple();
		ConjuntoTDA materiasSinOptativas = new Conjunto(); 
		ConjuntoTDA codigosDistintivos = new Conjunto();
		ConjuntoTDA materiasAisladas = new Conjunto();
		ConjuntoTDA totalMaterias = new Conjunto();
		GrafoTDA correlativas = new Grafo();
		
		materiasSinOptativas.inicializar();
		materiasCorrelativas.inicializar();
		registroDeMaterias.inicializar();
		codigosDistintivos.inicializar();
		correlativas.inicializarGrafo();
		materiasAisladas.inicializar();
		totalMaterias.inicializar();

		LectorDeArchivo.cargarDatos(registroDeMaterias, materiasSinOptativas, totalMaterias,correlativas,materiasCorrelativas);
		MetodosGrafo.cargarAristas(correlativas, materiasCorrelativas);
		
		System.out.println("Algunos de los resultados a continuación pueden diferir de lo esperado, debido a que hemos considerado "
				+ "las materias optativas como únicas en lugar de tomar su código como cantidad de las mismas");
		
		System.out.println("\n*** CANTIDAD DE MATERIAS POR CARRERA ***\n");
		MetodosFuncionales.cantidadMateriasPorCarrera(registroDeMaterias);

		System.out.println("\n*** PORCENTAJES DE MATERIAS DE INFORMATICA POR CARRERA (34XXX) ***\n");
		codigosDistintivos.agregar(Constantes.MAT_INFORMATICA);
		MetodosFuncionales.calcularPorcentajesMaterias(registroDeMaterias, codigosDistintivos);

		System.out.println("\n*** PORCENTAJES DE MATERIAS DE CIENCIAS BASICAS POR CARRERA (31XXX) ***\n");
		codigosDistintivos.inicializar();
		codigosDistintivos.agregar(Constantes.MAT_CS_BASICAS);
		MetodosFuncionales.calcularPorcentajesMaterias(registroDeMaterias, codigosDistintivos);

		System.out.println("\n*** PORCENTAJES DE MATERIAS DE CIENCIAS SOCIALES POR CARRERA (2XXXX y 33XXX) ***\n");
		codigosDistintivos.inicializar();
		codigosDistintivos.agregar(Constantes.MAT_CS_SOCIALES_G1);
		codigosDistintivos.agregar(Constantes.MAT_CS_SOCIALES_G2);
		MetodosFuncionales.calcularPorcentajesMaterias(registroDeMaterias, codigosDistintivos);

		System.out.println("\n*** CANTIDAD DE MATERIAS OPTATIVAS POR CARRERA ***\n");
		MetodosFuncionales.cantidadOptativas(registroDeMaterias);

		System.out.println("\n*** MATERIAS COMUNES A TODAS LAS CARRERAS ***\n");
		MetodosFuncionales.materiasComunes(materiasSinOptativas, registroDeMaterias);

		System.out.println("\n*** CARRERAS CON 80% DE MATERIAS COMUNES***\n");
		MetodosFuncionales.carrerasCon80PorcientoDeCoincidencia(registroDeMaterias);

		System.out.println("\n*** MATERIAS DE CARRERAS QUE NO COMPARTEN CON OTRAS CARRERAS(TIPO ESPECIALIDADES)***\n");
		MetodosFuncionales.materiasQueNoComparten(registroDeMaterias);

		System.out.println("\n*** CARRERAS CON 20% DE MATERIAS COMUNES***\n");
		MetodosFuncionales.carrerasCon20PorcientoDeCoincidencia(registroDeMaterias);

		System.out.println("\n*** MATERIAS NO COMUNES PARA CADA COMBINACIÓN DE MATERIAS: ***\n");
		MetodosFuncionales.materiasNoComunes(registroDeMaterias);
		
		System.out.println("\n*** MAPA DE CORRELATIVIDADES***\n");
		MetodosFuncionales.mostrarCorrelativas(correlativas);
		
		System.out.println("\n*** MATERIAS CON MAYOR CANTIDAD DE MATERIAS PRECEDENTES***\n");
		MetodosFuncionales.materiasMayorPrecedentes(correlativas);
		
		System.out.println("\n*** MATERIAS CON MAYOR CANTIDAD DE MATERIAS PRECEDENTES***\n");
		MetodosFuncionales.materiasMayorSubsiguientes(correlativas);
		
		System.out.println("\n*** MATERIAS QUE NO TIENEN MATERIAS PRECEDENTES NI SUBSIGUIENTES INMEDIATAS***\n");
		MetodosFuncionales.materiasSinPrecedentesNiSubsiguientes(correlativas,materiasAisladas); 
		MetodosConjunto.imprimirConjunto(materiasAisladas);
	}
}
