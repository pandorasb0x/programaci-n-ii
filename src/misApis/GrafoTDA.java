package misApis;


public interface GrafoTDA {


	void inicializarGrafo();
	void agregarArista(int v1,int v2,int p);
	void eliminarArista(int v1,int v2);
	void agregarVertice(int v);
	void eliminarVertice(int v);
	ConjuntoTDA vertices();
	boolean existeArista(int v1, int v2);
	int pesoArista(int v1, int v2);
	
}
