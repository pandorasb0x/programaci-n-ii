package misUtilidades;

public class Constantes {
	
   public final static String RUTA_ARCHIVO = "src/Materias.csv";
    
    /*Columnas en el csv*/
    public final static int NRO_CARRERA  = 0;
    public final static int COD_MATERIA  = 1;
    public final static int MATERIA_PRECEDENTE = 2;
    public final static int CARRERA = 3;
    public final static int MATERIA = 4;

    /*Identificación de materias según sus primeros dígitos*/
    public final static int MAT_INFORMATICA = 34;
    public final static int MAT_CS_BASICAS = 31;
    public final static int MAT_CS_SOCIALES_G1 = 33;
    public final static int MAT_CS_SOCIALES_G2 = 2;

}
