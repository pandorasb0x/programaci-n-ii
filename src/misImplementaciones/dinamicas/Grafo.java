package misImplementaciones.dinamicas;

import misApis.ConjuntoTDA;
import misApis.GrafoTDA;

public class Grafo implements GrafoTDA{

	class NodoV{
		int et;
		NodoV sigV;
		NodoA inicio;
	}
		
		class NodoA{
			int peso;
			NodoA sigA;
			NodoV dest;
		}

		NodoV inicio;


		public void inicializarGrafo(){
			inicio = null;	
		
		}
		
		
		private NodoV posVertice(int v){
			NodoV actual = inicio;
			while(actual.et != v){
				actual = actual.sigV;			
				}
			return actual;
		}
		
		
		//precond: deben existir los dos vertices
		public void agregarArista(int v1,int v2,int p){
			NodoV verticeOrigen = posVertice(v1);
			NodoV verticeDestino = posVertice(v2);
			NodoA actual = verticeOrigen.inicio;
			while (actual != null && actual.dest != verticeDestino){
				actual = actual.sigA;
			}
			if (actual == null) { //no existe arista
				
				NodoA nuevo = new NodoA();
				nuevo.peso = p;
				nuevo.dest = verticeDestino;
				
				nuevo.sigA = verticeOrigen.inicio;
				verticeOrigen.inicio = nuevo;
			}	
		}
		
		
		
		public void eliminarArista(int v1,int v2){
		
			NodoV verticeOrigen = posVertice(v1);
			NodoV verticeDestino = posVertice(v2);
			
			//busco lugar de la arista
			NodoA ant = null;
			NodoA actual = verticeOrigen.inicio;
			while (actual.dest != verticeDestino){
				ant = actual;
				actual = actual.sigA;		
			}
			
			if (ant == null){ //Arista en primer lugar
				verticeOrigen.inicio = verticeOrigen.inicio.sigA;	
			} else {
				ant.sigA = actual.sigA;			
			}
		}



		//precond: El vertice no debe existir
		public void agregarVertice(int v){
		
			NodoV nuevo = new NodoV();
			nuevo.et = v;
			nuevo.inicio = null;
			nuevo.sigV = inicio;
			inicio = nuevo;
		}
	
		
		public void eliminarVertice(int v){
			
			NodoV actualV = inicio;
			NodoV vertDest = posVertice(v);
			
			while (actualV != null){
				NodoA antA = null,	actualA = actualV.inicio;
				while (actualA != null && actualA.dest != vertDest){
					antA = actualA;
					actualA = actualA.sigA;
				}
				if (actualA != null){ //existe arista
					if (antA == null){ //1er arista
						actualV.inicio = actualV.inicio.sigA;
					} else { 
						antA.sigA = actualA.sigA;
					}
				}
				actualV = actualV.sigV;
			}
			
			NodoV ant = null;
			NodoV actual = inicio;
			while (actual.et != v){
				ant = actual;
				actual = actual.sigV; 
			}
			
			if (ant == null)
				inicio = inicio.sigV;
			else
				ant.sigV = actual.sigV;
			
		}
		
	
		
		public ConjuntoTDA vertices(){
			ConjuntoTDA resultado = new Conjunto();
			resultado.inicializar();
			NodoV actual = inicio;
			while (actual != null){
				resultado.agregar(actual.et);
				actual = actual.sigV;
			}
			return resultado;
		}
		
		public boolean existeArista(int v1, int v2){
			NodoV verticeOrigen = posVertice(v1);
			NodoV verticeDestino = posVertice(v2);
			NodoA actual = verticeOrigen.inicio;
			while (actual != null && actual.dest != verticeDestino){
				actual = actual.sigA;
			}
			return actual != null;
		}
			
		
		public int pesoArista(int v1, int v2){
			NodoV verticeOrigen = posVertice(v1);
			NodoV verticeDestino = posVertice(v2);
			NodoA actual = verticeOrigen.inicio;
			while (actual.dest != verticeDestino){
				actual = actual.sigA;
			}
			return actual.peso;
		}
}
