package misMetodos;

import misApis.ConjuntoTDA;
import misApis.DiccionarioMultipleTDA;
import misApis.GrafoTDA;
import misImplementaciones.dinamicas.Conjunto;


import java.text.DecimalFormat;

public class MetodosFuncionales {
	
	public enum TipoOperacion {
        EXACTLY, GREATEREQUAL;
    }
	
    /**  @Tarea Agregar en un diccionario múltiple, el par  código de carrera y  código de materia (1) y en un conjunto las materias.
     *  @Parámetros DiccionarioMultipleTDA, String, String.
     *  @Devuelve Void
     *  @Precondición El diccionario recibido, debe estar inicializado.
     *  @Postcondición El código de carrera y de materia se agregaron al diccionario múltiple.
     *  @Costo Esta dado por la función "agregar", cuyo costo es o(n)
     *  **/
    public static void registrarMateria(DiccionarioMultipleTDA dm, ConjuntoTDA materiasSinOptativas, ConjuntoTDA totalMaterias, GrafoTDA g, DiccionarioMultipleTDA d, String materiaPrecedente, String codigoCarrera, String codigoMateria){
        int codigo = Integer.parseInt(codigoCarrera);
        int materia = Integer.parseInt(codigoMateria);
       
        dm.agregar(codigo, materia);
        
        if(!materiasSinOptativas.pertenece(materia)) {
        	g.agregarVertice(materia);
        }
        
        materiasSinOptativas.agregar(materia);

        if (!materiaPrecedente.equals("")) {
        	int precedente = Integer.parseInt(materiaPrecedente);
        	d.agregar(materia, precedente);
        }
        }
        
    
    /**  @Tarea Informar la cantidad de materias de cada una de las carreras (2-a)
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void
     *  @Precondicion El diccionario recibido, no debe estar vacio.
     *  @Postcondicion Se muestra por pantalla cada carrera con su cantidad de materias.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es o(n)
     *  **/
    public static void cantidadMateriasPorCarrera(DiccionarioMultipleTDA dm) {
        int materiaElegida, carreraElegida, cantMat = 0;
        ConjuntoTDA carreras = dm.claves();
        ConjuntoTDA materias;

        while(!carreras.estaVacio()){
            carreraElegida = carreras.elegir();
            materias = dm.recuperar(carreraElegida);
            cantMat = cantidadMateriasPorCarrera(materias);
            System.out.println("Codigo de carrera: " + carreraElegida + " ---- Cantidad de materias: " + cantMat + "   ");
            carreras.sacar(carreraElegida);
        }
    }

    /**  @Tarea Calcular el porcentaje de materias de cada carrera, que posee determinados digitos en su codigo.
     *  @Parametros DiccionarioMultipleTDA, ConjuntoTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido y el conjunto de codigos no deben estar vacio.
     *  @Postcondicion Se muestra el porcentaje calculado por cada materia.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es o(n^2)
     *  **/
    public static void calcularPorcentajesMaterias(DiccionarioMultipleTDA dm, ConjuntoTDA codigosDistintivos) {
        int carreraElegida, totalMaterias, materiasSegunCod = 0;
        String resultado;
        ConjuntoTDA carreras = dm.claves();
        ConjuntoTDA materias;

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);

        while(!carreras.estaVacio()){
            carreraElegida = carreras.elegir();
            materias = dm.recuperar(carreraElegida);

            materiasSegunCod = cantMatSegunComienzos(materias, codigosDistintivos);
            totalMaterias = MetodosConjunto.tamanio(materias);

            carreras.sacar(carreraElegida);
            resultado = df.format(Float.valueOf(materiasSegunCod*100/(float)totalMaterias));
            System.out.println("Codigo de carrera: " + carreraElegida + " ---- Porcentaje: " + resultado + "   ");
        }
    }

   

    /**  @Tarea Calcular la cantidad de materias optativas de cada una de las carreras.
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido no debe estar vacio.
     *  @Postcondicion Se muestra la cantidad de materias optativas de cada una de las carreras.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es O(n^2)
     *  **/
    public static void  cantidadOptativas(DiccionarioMultipleTDA dm){
        int materiaElegida, carreraElegida, cantOpt = 0;
        ConjuntoTDA carreras = dm.claves();
        ConjuntoTDA materias;

        while(!carreras.estaVacio()){
            carreraElegida = carreras.elegir();
            materias = dm.recuperar(carreraElegida);
            cantOpt = cantidadOptativas(materias);
            System.out.println("Codigo de carrera: " + carreraElegida + " ---- Cantidad de optativas: " + cantOpt + "   ");
            carreras.sacar(carreraElegida);
        }
    }


    /**  @Tarea Mostrar cada par de carreras que coincidan en un 80% o m�s de materias.
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido no debe estar vacio.
     *  @Postcondicion Se muestra cada par de carreras que coincidan en un 80% o m�s de materias.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es O(n^3)
     *  **/
    public static void carrerasCon80PorcientoDeCoincidencia(DiccionarioMultipleTDA dm) {
        ConjuntoTDA carreras;
        ConjuntoTDA carrerasAux;
        ConjuntoTDA materiasCarreraElegida;
        ConjuntoTDA materiasCarreraAux;
        ConjuntoTDA materiasComunes;
        int carreraElegida;
        int carreraAuxElegida;

        carreras = dm.claves();
        carrerasAux = dm.claves();

        while(!carreras.estaVacio()) {
            carreraElegida = carreras.elegir(); //carrera a comparar
            materiasCarreraElegida = dm.recuperar(carreras.elegir()); //materias de la carrera a comparar
            while(!carrerasAux.estaVacio()) {
                carreraAuxElegida = carrerasAux.elegir();
                if(carreraElegida != carreraAuxElegida) { //chequeo no comparar la misma carrera
                    materiasCarreraAux = dm.recuperar(carreraAuxElegida);
                    materiasComunes = obtenerMateriasComunes(materiasCarreraElegida, materiasCarreraAux);
                    if(coincideEnXporcentaje( materiasCarreraElegida, materiasCarreraAux,  materiasComunes, 80, TipoOperacion.GREATEREQUAL))
                        System.out.println("La carrera " + carreraElegida + " tiene mas del 80% de materias comunes con " + carreraAuxElegida);
                }
                carrerasAux.sacar(carreraAuxElegida);
            }
            carreras.sacar(carreraElegida);
            MetodosConjunto.copiarConjunto(carreras, carrerasAux);
        }
    }
    
    /**  @Tarea Mostrar por cada carrera aquellas materias que no compartan con ninguna otra carrera.
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido no debe estar vacio.
     *  @Postcondicion Se muestran todas las materias �nicas por carrera.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es O(n^3)
     *  **/
    public static void materiasQueNoComparten(DiccionarioMultipleTDA dm) {
        ConjuntoTDA carreras;
        ConjuntoTDA carrerasAux;
        ConjuntoTDA materiasCarreraAux;
        ConjuntoTDA materiasPropias = null;
        int carreraElegida;
        int carreraAuxElegida;

        carreras = dm.claves();
        carrerasAux = dm.claves();

        while(!carreras.estaVacio()) {
            carreraElegida = carreras.elegir(); //carrera a comparar
            materiasPropias = dm.recuperar(carreras.elegir()); //materias de la carrera a comparar
            carrerasAux = dm.claves();
            while(!carrerasAux.estaVacio()) {
                carreraAuxElegida = carrerasAux.elegir();
                if(carreraElegida != carreraAuxElegida) { //chequeo no comparar la misma carrera
                    materiasCarreraAux = dm.recuperar(carreraAuxElegida);
                    materiasPropias = materiasNoContenidas(materiasPropias, materiasCarreraAux);
                }
                carrerasAux.sacar(carreraAuxElegida);
            }
            System.out.println("Para la carrera " + carreraElegida + ", las materias que NO son compartidas por otras carreras son:");
            MetodosConjunto.imprimirConjuntoOrdenado(MetodosConjunto.ordenarConjunto(materiasPropias));
            carreras.sacar(carreraElegida);
        }
    }
    
    /**  @Tarea Mostrar cada par de carreras que coincidan en un 20% o m�s de materias.
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido no debe estar vacio.
     *  @Postcondicion Se muestra cada par de carreras que coincidan en un 20% o m�s de materias.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es O(n^3)
     *  **/
    public static void carrerasCon20PorcientoDeCoincidencia(DiccionarioMultipleTDA dm) {
        ConjuntoTDA carreras;
        ConjuntoTDA carrerasAux;
        ConjuntoTDA materiasCarreraElegida;
        ConjuntoTDA materiasCarreraAux;
        ConjuntoTDA materiasComunes;
        int carreraElegida;
        int carreraAuxElegida;

        carreras = dm.claves();
        carrerasAux = dm.claves();

        while(!carreras.estaVacio()) {
            carreraElegida = carreras.elegir(); //carrera a comparar
            materiasCarreraElegida = dm.recuperar(carreras.elegir()); //materias de la carrera a comparar
            while(!carrerasAux.estaVacio()) {
                carreraAuxElegida = carrerasAux.elegir();
                if(carreraElegida != carreraAuxElegida) { //chequeo no comparar la misma carrera
                    materiasCarreraAux = dm.recuperar(carreraAuxElegida);
                    materiasComunes = obtenerMateriasComunes(materiasCarreraElegida, materiasCarreraAux);
                    if(coincideEnXporcentaje( materiasCarreraElegida, materiasCarreraAux,  materiasComunes, 20, TipoOperacion.EXACTLY))
                        System.out.println("La carrera " + carreraElegida + " exactamente 20% de materias comunes con " + carreraAuxElegida);
                }
                carrerasAux.sacar(carreraAuxElegida);
            }
            carreras.sacar(carreraElegida);
            MetodosConjunto.copiarConjunto(carreras, carrerasAux);
        }
    }

    
    /**  @Tarea Mostrar por cada par de carreras aquellas materias que no comparten (materias no comunes).
     *  @Parametros DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El diccionario recibido no debe estar vacio.
     *  @Postcondicion Se muestran las materias no comunes por cada par de carreras.
     *  @Costo Esta dado por la cantidad de materias por carrera, su costo es O(n^3)
     *  **/
    public static void materiasNoComunes(DiccionarioMultipleTDA dm) {
        int carreraElegida;
        int carreraAuxElegida;
        ConjuntoTDA materiasCarreraElegida;
        ConjuntoTDA materiasCarreraAux;
        ConjuntoTDA noComunes;
        ConjuntoTDA carreras = dm.claves();
        ConjuntoTDA carrerasAux = new Conjunto();
        carrerasAux.inicializar();

        while(!carreras.estaVacio()) {
            carreraElegida = carreras.elegir();
            materiasCarreraElegida = dm.recuperar(carreraElegida);
            MetodosConjunto.copiarConjunto(carreras, carrerasAux);
            while (!carrerasAux.estaVacio()){
                carreraAuxElegida = carrerasAux.elegir();
                if(carreraElegida!= carreraAuxElegida) {
                    materiasCarreraAux = dm.recuperar(carreraAuxElegida);
                    noComunes = obtenerMateriasNoComunes(materiasCarreraElegida, materiasCarreraAux);
                    System.out.println("Entre la carrera " + carreraElegida + " y la carrera " + carreraAuxElegida + ", las materias que NO son comunes son:");
                    MetodosConjunto.imprimirConjunto(noComunes);
                }
                carrerasAux.sacar(carreraAuxElegida);
            }
            carreras.sacar(carreraElegida);
        }
    }


    /**  @Tarea Mostrar las materias que tienen mayor cantidad de materias precedentes
     *  @Parametros GrafoTDA.
     *  @Devuelve Void.
     *  @Precondicion El grafo no debe estar vacio
     *  @Postcondicion Se muestran las materias que tienen mayor cantidad de materias precedentes
     *  @Costo Esta dado por la cantidad de vertices (materias) del grafo, su costo es O(n^2)
     *  **/
    public static void materiasMayorPrecedentes(GrafoTDA g) {
    	ConjuntoTDA materias = new Conjunto();
    	materias.inicializar();
    	int mayorCantidadPrecedentes = MetodosGrafo.cantidadMayorPrecedentes(g);
    	
    	int vo,vd,cantidad;
    	ConjuntoTDA vert = g.vertices();
    	
    	while (!vert.estaVacio()){
    		cantidad = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		ConjuntoTDA dest = g.vertices();
 	
    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vo,vd)){
    				cantidad++;
    			}
    		}
    		if(cantidad==mayorCantidadPrecedentes) {
    			materias.agregar(vo);
    		}
    	}
    	MetodosConjunto.imprimirConjunto(materias);
    }

    
    /**  @Tarea Mostrar las materias que tienen mayor cantidad de materias subsiguientes
     *  @Parametros GrafoTDA.
     *  @Devuelve Void.
     *  @Precondicion El grafo no debe estar vacio
     *  @Postcondicion Se muestran las materias que tienen mayor cantidad de materias subsiguientes
     *  @Costo  Esta dado por la cantidad de vertices (materias) del grafo, su costo es O(n^2)
     *  **/
    public static void materiasMayorSubsiguientes(GrafoTDA g) {
    
    	ConjuntoTDA materias = new Conjunto();
    	materias.inicializar();
    	int mayorCantidadSubsiguientes = MetodosGrafo.cantidadMayorSubsiguientes(g);
    	
    	int vo,vd,cantidad;
    	ConjuntoTDA vert = g.vertices();
    	
    	while (!vert.estaVacio()){
    		cantidad = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		ConjuntoTDA dest = g.vertices();
 	
    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vd,vo)){
    				cantidad++;
    			}
    		}
    		if(cantidad==mayorCantidadSubsiguientes) {
    			materias.agregar(vo);
    		}
    	}
    	
    	MetodosConjunto.imprimirConjunto(materias);
    }
    
    
    /**  @Tarea Cargar un conjunto con materias sin materias precedentes ni subsiguientes
     *  @Parametros GrafoTDA, ConjuntoTDA.
     *  @Devuelve Void.
     *  @Precondicion El grafo no debe estar vacio. El conjunto debe estar inicializado
     *  @Postcondicion El conjunto queda cargado con las materias que no tienen materias precedentes ni subsiguientes
     *  @Costo Esta dado por la cantidad de materias del conjunto, su costo es O(n^3)
     *  **/
    public static void materiasSinPrecedentesNiSubsiguientes(GrafoTDA g, ConjuntoTDA materias) {
    	int vo,vd,cantidad;
    	ConjuntoTDA vert = g.vertices();
    	while (!vert.estaVacio()){
    		cantidad = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		
    		ConjuntoTDA dest = g.vertices();
 	
    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vd,vo) || g.existeArista(vo, vd)){
    				cantidad++;
    			}
    		}
    		if(cantidad == 0) {
    			materias.agregar(vo);
    		}
    	}
    }


    /**  @Tarea Mostrar el grafo de materias correlativas
     *  @Parametros GrafoTDA.
     *  @Devuelve Void.
     *  @Precondicion El grafo no debe estar vacio
     *  @Postcondicion Se muestra por pantalla el grafo de materias correlativas
     *  @Costo Esta dado por la cantidad de vertices (materias) del grafo, su costo es O(n^3)
     *  **/
    public static void mostrarCorrelativas(GrafoTDA g){
    	ConjuntoTDA verticesAislados = new Conjunto();
    	verticesAislados.inicializar();
    	MetodosFuncionales.materiasSinPrecedentesNiSubsiguientes(g, verticesAislados);
    	int vo, vd,cont;
    	ConjuntoTDA vert = g.vertices();
    	while (!vert.estaVacio()){
    		cont = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		ConjuntoTDA dest = g.vertices();

    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vo,vd)){
    				if (cont == 0) {
    					System.out.println("Materia: " + vo);
    					System.out.println("Habilitada por: ");
    					cont++;
    				}
    				System.out.println(vd);
    			}
    		}
    	if (cont!=0){
    		System.out.println();
    	}
    	}
    }
    
 
   
    /**  @Tarea Devuelve la cantidad de materias de acuerdo a los codigos recibidos por parametros
     *  @Parametros ConjuntoTDA, ConjuntoTDA
     *  @Devuelve int
     *  @Precondicion Los conjuntos deben estar inicializados
     *  @Postcondicion Devuelve la cantidad de materias de acuerdo a los codigos recibidos por parametros
     *  @Costo  Esta dado por la cantidad de materias del conjunto, su costo es O(n^2)
     *  **/
    private static int cantMatSegunComienzos(ConjuntoTDA materias, ConjuntoTDA codigosDistintivos){
        int totalMatSegunCod = 0,codElegido;
        ConjuntoTDA auxCodDistintivos = new Conjunto();
        auxCodDistintivos.inicializar();
        MetodosConjunto.copiarConjunto(codigosDistintivos,auxCodDistintivos); //copio para trabajar con aux y no destruir.

        while (!auxCodDistintivos.estaVacio()){
            codElegido = auxCodDistintivos.elegir();
            totalMatSegunCod += cantMatPorCadaCodigo(materias, Integer.toString(codElegido));
            auxCodDistintivos.sacar(codElegido);
        }
        return totalMatSegunCod;
    }

    /**  @Tarea Devuelve la cantidad de materias de acuerdo al codigo recibido por parametro
     *  @Parametros ConjuntoTDA, String
     *  @Devuelve int
     *  @Precondicion El conjunto debe estar inicialziado
     *  @Postcondicion Devuelve la cantidad de materias de acuerdo al codigo recibido por parametro
     *  @Costo Esta dado por la cantidad de materias del conjunto, su costo es O(n)
     *  **/
    private static int cantMatPorCadaCodigo(ConjuntoTDA materias, String codigoComienzo) {
        int materiaElegida, contMat = 0;
        String buffer;
        ConjuntoTDA auxMaterias = new Conjunto();
        auxMaterias.inicializar();

        MetodosConjunto.copiarConjunto(materias,auxMaterias); //copio para trabajar con aux y no destruir.

        while(!auxMaterias.estaVacio()){
            materiaElegida = auxMaterias.elegir();
            buffer = Integer.toString(materiaElegida);
            if(buffer.startsWith(codigoComienzo) && buffer.length()==5)
                contMat ++;
            auxMaterias.sacar(materiaElegida);
        }
        return contMat;
    }
    
    /**  @Tarea Calcular la cantidad de materias optativas que hay en un conjunto de materias recibido por par�metro.
     *  @Par�metros ConjuntoTDA.
     *  @Devuelve int.
     *  @Precondici�n El diccionario recibido no debe estar vac�o.
     *  @Postcondici�n Se devuelve la cantidad de materias optativas.
     *  @Costo Esta dado por la cantidad de materias del conjunto, su costo es O(n)
     *  **/
    private static int cantidadOptativas(ConjuntoTDA materias) {
        int totalOptativas = 0,materiaElegida;
        ConjuntoTDA auxMaterias = new Conjunto();
        auxMaterias.inicializar();
        MetodosConjunto.copiarConjunto(materias,auxMaterias); //copio para trabajar con aux y no destruir.

        while (!auxMaterias.estaVacio()){
            materiaElegida = auxMaterias.elegir();
            if(esOptativa(materiaElegida))
                totalOptativas += materiaElegida;
            auxMaterias.sacar(materiaElegida);
        }
        return totalOptativas;
    }

    /**  @Tarea Calcular la cantidad de materias optativas que hay en un conjunto de materias recibido por par�metro.
     *  @Par�metros int.
     *  @Devuelve boolean.
     *  @Precondici�n -
     *  @Postcondici�n Se devuelve true si la materia es optativa.
     *  @Costo Constante (O(1))
     *  **/

    private static boolean esOptativa(int materia){
        return   Integer.toString(materia).length() != 5; // los codigos de optativas no tienen 5 digitos
    }

    
    /**  @Tarea Muestra las materias comunes a todas las carreras.
     *  @Par�metros ConjuntoTDA, DiccionarioMultipleTDA.
     *  @Devuelve void.
     *  @Precondici�n Conjunto y diccionario inicializados.
     *  @Postcondici�n Se muestran las materias comunes a todas las carreras
     *  @Costo Esta dado por la cantidad de materias de las carreras, su costo es O(n^2)
     *  **/
    public static void materiasComunes(ConjuntoTDA materiasSinOptativas, DiccionarioMultipleTDA dm) {
        int carreraElegida;
        ConjuntoTDA materias;
        ConjuntoTDA carreras = dm.claves();
        ConjuntoTDA materiasComunes = new Conjunto();
        materiasComunes.inicializar();

        MetodosConjunto.copiarConjunto(materiasSinOptativas,materiasComunes);
        while(!carreras.estaVacio()) {
            carreraElegida = carreras.elegir();
            materias = dm.recuperar(carreraElegida);
            materiasComunes = obtenerMateriasComunes(materias,materiasComunes);
            carreras.sacar(carreraElegida);
        }
        MetodosConjunto.imprimirConjuntoOrdenado(MetodosConjunto.ordenarConjunto(materiasComunes));
    }
    
    
    /**  @Tarea Dados 3 conjuntos recibidos por par�metro, un porcentaje y un tipo de operacion (si es >= o =), verifica si en alg�n caso se cumple el "X" porcentaje de coincidencia
     *  @Par�metros ConjuntoTDA, ConjuntoTDA, ConjuntoTDA, int, TipoOperacion
     *  @Devuelve boolean.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve true si cumple con el porcentaje.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibido, ya que se calcula su tama�o, su costo es O(n^2)
     *  **/
    private static boolean coincideEnXporcentaje(ConjuntoTDA materiasCarreraElegida, ConjuntoTDA materiasCarreraAux, ConjuntoTDA materiasComunes, int porcentaje, TipoOperacion tipoOperacion){
        boolean coincide = false;
        int cantMatCarreraElegida = MetodosConjunto.tamanio(materiasCarreraElegida);
        int cantMatCarreraAux = MetodosConjunto.tamanio(materiasCarreraAux);
        int cantMatComunes = MetodosConjunto.tamanio(materiasComunes);

        float relacion1 = (float)cantMatComunes/cantMatCarreraElegida;
        float relacion2 = (float)cantMatComunes/cantMatCarreraAux;

        if (obtenerSegunTipoOperacion(porcentaje, relacion1, tipoOperacion) || obtenerSegunTipoOperacion(porcentaje, relacion2, tipoOperacion)) //compruebo a ambos lados
            coincide = true;
        return coincide;
    }

    /**  @Tarea Verifica si una relacion recibida por parametro cumple el porcentaje segun el tipo de operacion
     *  @Par�metros int, float, TipoOperacion
     *  @Devuelve boolean.
     *  @Precondici�n -
     *  @Postcondici�n Se devuelve true si la relacion cumple el criterio
     *  @Costo Constante (O(1))
     *  **/
    private static boolean obtenerSegunTipoOperacion(int porcentaje, float relacion, TipoOperacion tipoOperacion) {
        switch (tipoOperacion){
            case EXACTLY:
                return relacion == porcentaje / 100;
            case GREATEREQUAL:
                return relacion >= porcentaje / 100;
            default:
                return false;
        }
    }

    /**  @Tarea Dado un conjunto de materias recibido por par�metro, calcula la cantidad de materias que contiene.
     *  @Par�metros ConjuntoTDA.
     *  @Devuelve int.
     *  @Precondici�n El conjunto debe estar inicializado
     *  @Postcondici�n Se devuelve el tama�o del conjunto.
     *  @Costo  Esta dado por la cantidad de materias del conjuntos recibido, ya que se calcula su tama�o, su costo es O(n^2)
     *  **/
    private static int cantidadMateriasPorCarrera(ConjuntoTDA materias) {
        return MetodosConjunto.tamanio(materias);
    }

    
    /**  @Tarea Dado un conjunto c1 y c2 recibidos por par�metro, devuelve un conjunto con las materias que son propias de c1 pero que no est�n en c2 (o sea la diferencia).
     *  @Par�metros ConjuntoTDA, ConjuntoTDA.
     *  @Devuelve ConjuntoTDA.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve el tama�o del conjunto.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibidos, su costo es O(n)
     *  **/
    private static ConjuntoTDA materiasNoContenidas(ConjuntoTDA materiasCarreraElegida, ConjuntoTDA materiasCarreraAux){
        return MetodosConjunto.diferencia(materiasCarreraElegida, materiasCarreraAux);
    }

    /**  @Tarea Devolver elementos contenidos en ambos conjuntos.
     *  @Par�metros ConjuntoTDA c1, ConjuntoTDA c2.
     *  @Devuelve ConjuntoTDA
     *  @Precondici�n El conjunto c1 y c2 deben estar inicializados y no vac�os.
     *  @Postcondici�n Se devuelve un conjunto que contiene la intersecci�n entre los conjuntos recibidos por par�metro
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/
    private static ConjuntoTDA obtenerMateriasComunes(ConjuntoTDA materias, ConjuntoTDA materiasComunes){
        return MetodosConjunto.interseccion(materias,materiasComunes);
    }

    
    /**  @Tarea Dado un conjunto c1 y c2 recibidos por par�metro, devuelve un conjunto con las materias que son propias de c1 y tambi�n c2 pero sin su intersecci�n
     *  @Par�metros ConjuntoTDA, ConjuntoTDA.
     *  @Devuelve ConjuntoTDA.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve el conjunto que contiene la diferencia sim�trica entre ambos conjuntos.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibidos, su costo es O(n)
     *  **/
    private static ConjuntoTDA obtenerMateriasNoComunes(ConjuntoTDA materias, ConjuntoTDA materiasComunes){
        return MetodosConjunto.diferenciaSimetrica(materias,materiasComunes);
    }
}

    
    
    

