package misMetodos;

import misApis.ConjuntoTDA;
import misApis.DiccionarioMultipleTDA;
import misApis.GrafoTDA;


public class MetodosGrafo {

	
	/**  @Tarea Carga las aristas de los grafos
     *  @Parametros GrafoTDA, DiccionarioMultipleTDA.
     *  @Devuelve Void.
     *  @Precondicion El grafo debe tener cargado los vertices. El diccionario m�ltiple debe estar cargado.
     *  @Postcondicion Las aristas del grafo quedan cargadas con peso 1.
     *  @Costo Esta dado por la cantidad de materias del diccionario multiple, su costo es O(n^2)
     *  **/
	public static void cargarAristas(GrafoTDA g, DiccionarioMultipleTDA d) {
    	
    	ConjuntoTDA claves, valores, verticesGrafo;
    	int clave,valor;
    	claves = d.claves();
    	verticesGrafo = g.vertices();
    	while(!claves.estaVacio()) {
    		clave = claves.elegir();
    		valores = d.recuperar(clave);
    		while (!valores.estaVacio()) {
    			valor = valores.elegir();
    			if (!verticesGrafo.pertenece(valor)) {
    				g.agregarVertice(valor);
    			}
    			g.agregarArista(clave,valor, 1);
    			valores.sacar(valor);
    		}
    		claves.sacar(clave);
    	}
    	}

    	
	/**  @Tarea Calcular la cantidad maxima de materias precedentes 
     *  @Parametros GrafoTDA,
     *  @Devuelve int
     *  @Precondicion El grafo debe estar cargado
     *  @Postcondicion Devuelve la cantidad maxima de materias precedentes
     *  @Costo Esta dado por la cantidad de vertices (materias) del grafo, su costo es O(n^2)
     *  **/
    public static int cantidadMayorPrecedentes(GrafoTDA g) {
    	int vo, vd;
    	int cantidad;
    	int cantidadMayor = 0;
    	
    	ConjuntoTDA vert = g.vertices();
    	
    	while (!vert.estaVacio()){
    		cantidad = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		ConjuntoTDA dest = g.vertices();
 	
    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vo,vd)){
    				cantidad++;
    			}
    		}
    		if(cantidad>cantidadMayor) {
    			cantidadMayor = cantidad;
    		}
    	}
    	return cantidadMayor;
    }
	
	
    /**  @Tarea Calcular la cantidad maxima de materias subsiguientes
     *  @Parametros GrafoTDA
     *  @Devuelve int
     *  @Precondicion El grafo debe estar cargado
     *  @Postcondicion Devuelve la cantidad maxima de materias subsiguientes
     *  @Costo Esta dado por la cantidad de vertices (materias) del grafo, su costo es O(n^2)
     *  **/
    public static int cantidadMayorSubsiguientes(GrafoTDA g) {
    	int vo, vd;
    	int cantidad;
    	int cantidadMayor = 0;
    	
    	ConjuntoTDA vert = g.vertices();
    	
    	while (!vert.estaVacio()){
    		cantidad = 0;
    		vo = vert.elegir();
    		vert.sacar(vo);
    		ConjuntoTDA dest = g.vertices();
 	
    		while(!dest.estaVacio()){
    			vd = dest.elegir();
    			dest.sacar(vd);
    			if (g.existeArista(vd,vo)){
    				cantidad++;
    			}
    		}
    		if(cantidad>cantidadMayor) {
    			cantidadMayor = cantidad;
    		}
    	}
    	return cantidadMayor;
    }
	
	
}
